# Tag 01

**Gemäss Stundenplan: vor Ort oder Fernunterricht**

## Lektion 01
### Aufstellung/ Abfrage Azure Kenntnisse
- Skala 0 bis 10 aufstellen und die STUD sollen sich entsprechend Kenntnisse verteilen
- STUD fragen, weshalb sie gerade dort stehen?
    - STUD stellen sich persönlich vor
    - Nachfragen, welche Erwartungen/ Bedürfnisse sie vom Kurs haben
    - www.mentimeter.com einsetzen und die Begriffe bezüglich Erwartungen/ Bedürfnisse

### Coaches stellen sich vor
- Coaches stellen sich einzeln vor

## Administratives
- MS Teams
- Stundenplan
- Pausenregelung
    - Hands-on/ Gruppenarbeit, Pausen selbst regeln
- Modulaufbau/ Übersicht -> Vor allem Hands-on -> vor allem basierend AZ-900 (Azure Fundamentals)
    - Ggf. eigene hands-on Aufträge (Wissen vertiefen)
    - Es gibt diverse Kurse/Learning Paths auf learn.microsoft.com
        - Azure Administrator:      https://learn.microsoft.com/en-us/certifications/exams/az-104/
        - Azure Developer:          https://learn.microsoft.com/en-us/certifications/exams/az-204/
        - Azure Solution Architect: https://learn.microsoft.com/en-us/certifications/exams/az-305/
        - und noch viele mehr...
    - Info: MS Certified: Azure Solution Architect Expert setzt AZ-104 voraus
- Distance-Learning: 
    - Einführung/ Ziele
    - Hands-on
        - Während online-session jederzeit Fragen an Coaches stellen
    - Am Ende Q&A
- Themen Parkplatz
    - MS Teams Channel
    - Bei bestimmten Fragen, Themen auflisten
    - Im besten Fall, am nächsten Unterrichtstag beantworten

## Lektion 02-03
- 6 Gruppen bilden (2 pro Modul)
- Cloud Grundlagen: Describe cloud concepts 
- Selbstständig einlesen
    - https://learn.microsoft.com/de-de/certifications/exams/az-900/
    - Gruppe 1/2: https://learn.microsoft.com/en-us/training/modules/describe-cloud-compute/
    - Gruppe 3/4: https://learn.microsoft.com/en-us/training/modules/describe-benefits-use-cloud-services/
    - Gruppe 5/6: https://learn.microsoft.com/en-us/training/modules/describe-cloud-service-types/
- Gruppenpuzzle umsetzen
    - https://lehrerfortbildung-bw.de/st_kompetenzen/weiteres/projekt/projektkompetenz/methoden_a_z/gruppenpuzzle/
- Am Ende:
    - Frage in die Runde: Welche Clouddiensttypen (cloud service types) kommen bei euch vor/ welchen Wunsch habt ihr?

## Lektion 04
- Azure Students Account (TBZ), Budget 100$ (1 Jahr)
    - Login gleich Testen -> https://azureforeducation.microsoft.com/devtools
- Hands-on Uebung anfangen: *Azure Architecture and Services*
    - https://learn.microsoft.com/en-us/training/modules/describe-core-architectural-components-of-azure/
- Logischer Aufbau Tenant-Managementgroup-Subscription-Ressourcegroup-Ressource
- Kenntnisse abfragen
- Feedback einholen
