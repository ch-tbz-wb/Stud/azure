# Tag 02

**Gemäss Stundenplan: vor Ort oder Fernunterricht**

## Lektion 01-02
- Recap Runde Tag 01
  - Quiz
- Q&A
- Azure Students Account (TBZ), Budget 100$ (1 Jahr)
  - Login gleich Testen -> https://azureforeducation.microsoft.com/devtools
- Hands-on Uebung anfangen: *Azure Architecture and Services*
    - https://learn.microsoft.com/en-us/training/modules/describe-core-architectural-components-of-azure/
    - Recap *Azure Architecture and Services* 
    - Regions/ Availibility Zones/ Datacenters
      - [Global Infrastructure](https://infrastructuremap.microsoft.com/)
      - [Datacenter locations](https://dgtlinfra.com/microsoft-azure-data-center-locations/)
      - [Datacenter underwater](https://www.crn.com/news/data-center/microsoft-s-underwater-data-center-a-success-azure-ahead)
    - Kurze Führung durchs Azure-Portal

## Lektion 03-04
- Auftrag Ressource erstellen
- Q&A