# Tag 03

**Gemäss Stundenplan: vor Ort oder Fernunterricht**

## Lektion 01-04
- Recap Runde Tag 02
  - Quiz
- Q&A
- Hands-on Uebung anfangen: *Compute & Networking*
  - https://learn.microsoft.com/en-us/training/modules/describe-azure-compute-networking-services/
  - Recap *Compute & Networking*
  - Auftrag [Azure Container APP](../../2_Unterrichtsressourcen/Auftraege/containerapp.md)
- Abschluss
- Q&A
