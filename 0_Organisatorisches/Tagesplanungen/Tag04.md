# Tag 04

**Gemäss Stundenplan: vor Ort oder Fernunterricht**

## Lektion 01-04
- Recap Runde Tag 03
  - Quiz
- Q&A
- Hands-on Uebung anfangen: *Azure Storage Services*
  - https://learn.microsoft.com/en-us/training/modules/describe-azure-storage-services/
  - Recap *Azure Storage Services*
  - Auftrag [Storage Account](../../2_Unterrichtsressourcen/Auftraege/storage%20account.md)
- Abschluss
- Q&A
