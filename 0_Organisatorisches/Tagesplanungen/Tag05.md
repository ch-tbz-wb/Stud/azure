# Tag 05

**Gemäss Stundenplan: vor Ort oder Fernunterricht**

## Lektion 01-04
- Recap Runde Tag 04
  - Quiz
- Q&A
- Hands-on Uebung anfangen: *Azure identity, access, and security*
  - https://learn.microsoft.com/en-us/training/modules/describe-azure-identity-access-security/
  - Recap *Azure identity, access, and security*
  - [challenge lab Configure Azure Role Based Access Control](../../2_Unterrichtsressourcen/Auftraege/Role%20base%20access%20control.md)
- Hands-on anfangen: *cost management in azure*
  - https://learn.microsoft.com/en-us/training/modules/describe-cost-management-azure/
  - Recap *cost management in azure*
  - [Auftrag *cost management*](https://gitlab.com/ch-tbz-wb/Stud/azure/-/blob/main/2_Unterrichtsressourcen/Auftraege/Cost%20Management.md) 
- Hands-on anfangen: *features and tools in Azure for governance and compliance*
  - https://learn.microsoft.com/en-us/training/modules/describe-features-tools-azure-for-governance-compliance/
  - Recap *features and tools in Azure for governance and compliance*
  - ~~[Auftrag *features and tools governance compliance*](https://gitlab.com/ch-tbz-wb/Stud/azure/-/blob/main/2_Unterrichtsressourcen/Auftraege/Governance%20and%20Compliance.md)~~
- Hands-on anfangen: *features and tools for managing and deploying Azure resources*
  - https://learn.microsoft.com/en-us/training/modules/describe-features-tools-manage-deploy-azure-resources/
  - Recap *features and tools for managing and deploying Azure resources*
  - [Auftrag *ARM Deployment*](https://gitlab.com/ch-tbz-wb/Stud/azure/-/blob/5565b22219b31987d013bda94fa1199b9de10b00/2_Unterrichtsressourcen/Auftraege/ARM%20Deployment.md)
- Hands-on anfangen: *monitoring tools in Azure*
  - https://learn.microsoft.com/en-us/training/modules/describe-monitoring-tools-azure/
  - Recap *monitoring tools in Azure* 
- Abschluss
- Q&A
