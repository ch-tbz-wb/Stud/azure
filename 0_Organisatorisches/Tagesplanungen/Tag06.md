# Tag 06

**Gemäss Stundenplan: vor Ort oder Fernunterricht**

## Lektion 01-05
- Fragen zu Tag 05 (allenfalls bilateral)
    - Cost management
    - Governance and compliance
    - Resource deployment tools
    - Monitoring tools
- Selbstgesteuertes Lernen
    - Semesterarbeit
    - Pendente Aufträge Tag01-05
    - Neue Aufträge, u.a. AI-Services
    - Eigene Themen?
