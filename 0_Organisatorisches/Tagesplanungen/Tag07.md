# Tag 07

**Gemäss Stundenplan: vor Ort oder Fernunterricht**

## Lektion 01-05
- Recap [Modul03 management and governance](https://gitlab.com/ch-tbz-wb/Stud/azure/-/blob/main/2_Unterrichtsressourcen/Modul03/EN/Module03%20management%20and%20governance.pdf?ref_type=heads)
- Selbstgesteuertes Lernen
    - Semesterarbeit
    - Pendente Aufträge Tag01-05
    - Neue Aufträge, u.a. ARM-Deployment, AI-Services, usw.