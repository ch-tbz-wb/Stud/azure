# Handlungskompetenzen

## Modulspezifische Handlungskompetenzen
- A1 Unternehmens- und Führungsprozesse gestalten und verantworten
  - A1.3 Fachliche Kenntnisse kombiniert mit betriebswirtschaftlichem Wissen für einen ökonomisch, ökologisch und sozial erfolgreichen Geschäftsgang einsetzen (Niveau: 2)
- B4 Entwicklungsmethoden zur Lösung von ICT-Problemen und Entwicklung von ICT-Innovationen zielführend einsetzen
  - B4.2 ICT-Problemstellungen unter Berücksichtigung vernetzten Denkens, Entwicklung neuer ICT-Lösungen und Anwendung aktueller Technologien identifizieren, analysieren und lösen (Niveau: 3)
  - B4.6 Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen (Niveau: 3)
  - B4.7 Informationsquellen und Wissensnetzwerke kritisch reflexiv nutzen (Niveau: 3)
- B5 ICT-Projekte und Vorhaben planen, leiten, umsetzen und evaluieren
  - B5.9 Dokumente, die die Überwachung des Projektfortschritts erleichtern, erstellen und pflegen (Niveau: 3)
- B11 Applikationen entwickeln, Programme erstellen und testen
  - B11.1 Vorgaben für die Konzipierung eines Softwaresystems mit einer formalen Methode analysieren (Niveau: 3)
  - B11.2 Systemspezifikation interpretieren und die technische Umsetzung entwerfen (Niveau: 3)
  - B11.3 Spezifikation in einer geeigneten Programmiersprache umsetzen (Niveau: 3)

## Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen. Die Handlungskompetenzen werden in vier Anforderungsniveaus eingestuft.

### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledigen die Arbeiten selbständig unter Verantwortung einer Drittperson.

### Kompetenzniveau 3: Kompetenz professionellen Handelns

Bearbeiten neue komplexe Aufgaben und Problemstellungen in einem nicht vorhersehbaren Arbeitskontext; übernehmen die operative Verantwortung und planen, handeln und evaluieren autonom.

### Kompetenzniveau 4: Kompetenzexpertise

Entwickeln innovative Lösungen in einem komplexen Tätigkeitsfeld; antizipieren Veränderungen in der Zukunft und handeln proaktiv; übernehmen strategische Verantwortung und treiben Veränderungen und Entwicklungen voran.
