# Auftrag Chatbot (Question answering) basierend Azure Cognitive Service for Language

[TOC]

Zeit: 4-5 Lektionen<br>
Form: Alleine oder Teamarbeit

>
> Ziel dieser Übung ist, dass ihr euch mit dem Thema Cognitive Services auseinandersetzt, die dazugehörigen Ressourcetypen verwendet und die verschiedenen Azure Komponenten für eure Lösungsfindung einsetzt.
>

## Einleitung
AI bzw. Chatbots sind schon lange ein Thema, erfahren jetzt dank Large Language Models wie ChatGPT eine noch grössere Aufmerksamkeit. Es gibt von Microsoft einen  Service, welcher einfach und ohne Coding bereitgestellt werden kann.
Der Service hiess ursprünglich 'QnA Maker', wurde zu 'question answering' rebranded und in den Azure Cognitive Service for Language integriert.
Mit diesem Service könnt ihr verschiedene Informationen aus Dateien, Webseiten oder eigenen Frage / Antwort Kombinationen erfassen.
Anschliessend kann auf dieses Modell oder Chatbot mittels den verschiedensten Kanälen zugegriffen werden. Zum Beispiel Web Chat oder auch Microsoft Teams.

Anwendungsbereiche können sein: 
- Knowledge-Base für den Help-/Servicedesk. Sowohl für interne Mitarbeitende als auch Kunden.
- Produktinformationen für eure Kunden

[What is question answering?](https://learn.microsoft.com/en-us/azure/cognitive-services/language-service/question-answering/overview)

## Anmerkung
- Anmerkung: Es kann sein, dass ihr im TBZ-Tenant wegen der Erstellung einer User-assigned managed identity (bzw. den fehlenden Berechtigung dies zu tun) diese Übung nicht komplett durchführen könnt. Ihr könnt jedoch eure Free-Subscription auf einen privaten Account transferieren.


## Bereitstellung Language Service & Projekt
Hier findet ihr eine Anleitung, welche durch das Deployment führt:<br>
https://language.cognitive.azure.com/
- Kategorie: Understand questions and conversational language
- Typ: Custom question answering


## Lösungsansatz bzw. Vorgehen
Add source
- Wenn ihr den Language Service deployed und ein Projekt erstellt habt, werdet ihr zu "Manage sources" geleitet.
- Manage sources: Füge via 'Add source' einen 'chit chat' hinzu. Anschliessend wechsle zur Knowledge Base

Knowledge base
- Edit knowledge base: sieh dir die Fragen / Antworten Paare an. Wenn du willst, kannst du ein eigenes Paar erfassen.
- Deploy knowledge base: Deploye deine Knowledge Base. Dieser Vorgang kann einige Sekunden bis Minuten dauern.
- Gratuliere, du hast somit ein AI-basiertes Modell bereitgestellt. :-)

Bot deployment
- Nach dem Deployment wirst du auf der Seite einen neuen Schritt sehen "Next steps: Create a bot".
- Lies die Dokumentation durch [Link](https://aka.ms/qna-create-bot) und starte dann mit 'Create a bot'
- Ergänze die Informationen für das Custom Deployment. Achte darauf, möglichst günstige (oder gratis) SKUs zu verwenden.
- Achtung: Im Feld 'Language Resource Key' muss der Ocp-Apim-Subscription-Key hinterlegt werden.

Bot testen
- Öffne nach dem Deployment die Ressourcengruppe des Bots und klicke auf den deployed "Azure Bot"
- Navigiere zu "Settings" - "Test in Web Chat"
- Stelle eine Frage aus der Knowledge Base. Beispielsweise deine eigene Frage, falls du eine erstellt hast.

Channel hinzufügen
- Hat alles geklappt? Falls ja, kannst du mal einen neuen Channel hinzufügen. Zum Beispiel Microsoft Teams.
- Nach dem hinzufügen des neuen Channels, kannst du ihn gleich mal ausprobieren. Gehe auf den neuen Channel und wähle "Open in Teams"
- Anmerkung: Für die 'richtige' Bereitstellung im Microsoft Teams muss ein Paket erstellt und im M365 Tenant published werden. Diesen Schritt lassen wir zum jetzigen Zeitpunkt aus. 

Füge weitere Sources hinzu und spiel damit rum
- Du kannst nun weitere Sources hinzufügen bzw. bestehende entfernen.
- Experimentiere und finde heraus, was die Lösung kann und was nicht.
- Gibt es allenfalls Einschränkungen, aufgrund einer der gewählten SKUs?


# Dokumentation
Es ist euch überlassen, ob ihr diese Übung dokumentiert oder nicht. Wir empfehlen es euch jedoch.
Beispielsweise falls ihr euch überlegt, den question answering deinem Chef zu zeigen, weil ihr noch keine Knowledge Base mit MS Teams-Zugriff habt. Oder ihr euren internen Helpdesk entlasten wollt, durch die Bereitstellung von FAQ mit diesem Bot.