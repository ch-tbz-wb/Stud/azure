# Cost Management

Zeit: 15 Min.<br>
Form: Alleine

## Anforderung

- Dein Teamleader hat dich angerufen. Die Kosten für deine Ressourcengruppe sind letzten Monat über Budget.
- Er möchte von dir, dass du eine Auswertung für den aktuellen Monat machst. Er will die tägliche Kostenschwankung sehen.
- Weiter denkt er, dass die AppServicePlans bedeutend teurer sind als die Storage Accounts. Daher will er eine Trennung der Ressourcen Typen.

- Als Zückerli hast du ab sofort die Aufgabe, ihm monatlich einen Auszug dieses Reports per eMail zu schicken. Am liebsten als Excel, damit er gleich Pivotieren kann..


## Auftrag

1. Erstelle eine CostAnalysis View, welche die obige Anforderungen abdeckt. Dokumentiere die einzelnen Schritte in deinem E-Portfolio
2. Überleg dir, wie du die "Zückerli" Aufgabe umsetzen könntest.

---
Quellen / Zusätzliche Links:<br>
[Analyze cost with the Cost Management Power BI App for Enterprise Agreements (EA)](https://learn.microsoft.com/en-us/azure/cost-management-billing/costs/analyze-cost-data-azure-cost-management-power-bi-template-app) -> Für Unternehmen mit EA. Lizenzierung und notwendige Permissions (EA Admin) beachten!
