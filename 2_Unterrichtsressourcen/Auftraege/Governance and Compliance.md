# Governance and Compliance

Zeit: 30 Min.<br>
Form: Alleine

Folgeauftrag zu [Cost Management.md](https://gitlab.com/ch-tbz-wb/Stud/azure/-/blob/main/2_Unterrichtsressourcen/Auftraege/Cost%20Management.md?ref_type=heads)

## Anforderung

- Dein Teamleader hat erneut angerufen. Er hat gelesen, dass man im Cost Analysis nach Tags filtern kann.
- Er bittet dich, aber sofort für jede Ressourcengruppe einen Tag "Kostenstelle" zu erstellen.
- Für deine bestehende Ressourcengruppe ist der Value '19324'
- Zudem möchte er von dir, dass dieser Tag-Value auf alle Ressourcen in der Ressourcengruppe automatisiert übernommen wird.


## Auftrag

1. Setze den Tag nach obigen Vorgaben. Dokumentiere die einzelnen Schritte in deinem E-Portfolio
2. Finde einen Weg die Anforderung zu erfüllen. Dokumentiere die einzelnen Schritte in deinem E-Portfolio
3. Probiere sowohl Portal als auch CLI/Powershell aus
4. Wurde der Tag "Kostenstelle" vererbt? Ändere die Konfiguration einer Ressource und überprüfen, was mit den Tags auf dieser Ressource geschieht.
4. Aktualisiere den Tag "Kostenstelle" mit einem anderen Value. Was stellst du fest?


---
Hinweis: 
Append a tag and its value from the resource group
