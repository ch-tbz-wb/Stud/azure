# Auftrag Neue Webseite für die technische Berufsschule Zürich

[TOC]

Zeit: 6-8 Lektionen<br>
Form: Alleine oder Teamarbeit

>
> Ziel dieser Übung ist, dass ihr basierend verschiedener Anforderungen eine Lösung erarbeitet / dokumentiert und bereitstellt. 
>

## Einleitung
Du arbeitest als Cloud native-Engineer bei einem IT-Dienstleister. 
Einer deiner Kunden, die Technische Berufsschule Zürich, möchte einen neuen Webauftritt von euch entwickeln lassen. Du bist im Team mit dabei und hast diverse Aufgaben zu erledigen.
Du bist beispielsweise für die Evaluation der Azure Services, das Lösungsdesign und anschliessend auch die Bereitstellung verantwortlich.
Den Betrieb der Lösung wird die TBZ mehrheitlich selber machen. Deine Firma wird nur bei komplexen Problemen auf Stundenbasis engagiert. 


## Anforderungen der TBZ
- Es wird eine produktive Webseite und eine Schulungs- bzw. Übungswebseite für die Content Creators benötigt.
- Die Entwicklung neuer Features darf keinen Einfluss auf eine der beiden Webseiten haben und muss daher in einer getrennte/eigenen Umgebung erfolgen.
- Es braucht eine Benachrichtigung, wenn die Webseite nicht verfügbar sein sollte.
- Die Auslastung der Webseite bzw. der eingesetzten Services soll einfach erkennbar sein.
- Die Bereitstellung soll im TBZ Azure Tenant erfolgen.
- Der Betrieb der Lösung soll möglichst günstig sein.
- Eine Sicherung (Backup) der Website ist notwendig, muss aber nicht länger als einige Tage vorgehalten werden

## Weitere Anforderungen oder Rahmenbedingungen
- Es wird ein Lösungsdesign benötigt, welches die verwendeten Services aufzeigt (Highlevel Übersicht genügt - bspw. mittels https://draw.io).
- Es wird eine Zugriffsmatrix für die Services benötigt, welches die folgenden Rollen beinhaltet: Webseiten Administrator, Content Creator, Entwicklung. 
- Die Bereitstellung der Ressourcen muss mit IaC erfolgen.
- Dein Teamleader hat dich informiert, dass ein identisches Projekt bei einem anderen Kunden offeriert wurde.
- Beachte existierende Best-Practices vom Hersteller und dokumentiere diese
- Dokumentiere, wie du den Betrieb der Lösung kosteneffizient gestaltest
- Dokumentiere, was du im Monitoring integrierst und weshalb, sowie an wen du die Alerts schickst 


## Anmerkung
Wenn euch Informationen für einen Schritt fehlen, könnt ihr Annahmen treffen oder uns Fragen.
Die Bearbeitungsreihenfolge ist euch überlassen. Empfohlen wird mit der Evaluation der benötigten Services und dem Lösungsdesign anzufangen.


## "Präsentation"
Wer will, darf seine Arbeit gerne mit uns besprechen und unser Feedback einholen!


## Beispiele
Lösungsdesign
![](../../x_gitressourcen/Lösungsdesign_Beispiel.png)

Rollen- und Berechtigungskonzept
https://docs.datenschutz.ch/u/d/informationssicherheit/konzept/abweichend/vsa/beispiel_rollen_und_berechtigungskonzept_in_volksschulen.docx


