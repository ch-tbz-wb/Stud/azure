# Aufträge

- [Ressource erstellen](Ressource%20erstellen.md)
  - [Ressource erstellen advanced Part1](Ressource%20erstellen%20advanced%20Part1.md)
  - [ARM Deployment](ARM%20Deployment.md)
- [containerapp](containerapp.md)
- [storage account](storage%20account.md)
- [Cost Management](Cost%20Management.md)
- [Governance and Compliance](Governance%20and%20Compliance.md)
- [IOT mxchip AZ3166](iot%20mxchip%20AZ3166.md)
- [Dokumentenverteiler mit Form Recognizers](form%20recognizer.md)
- [Neue Webseite für einen Kunden](Neue%20Webseite.md)
- [Chatbot basierend Azure Language Servce (NoCode)](Chatbot.md)
---
