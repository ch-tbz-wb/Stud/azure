# Ressource erstellen advanced Part1

Zeit: 90 Min.<br>
Form: 2er Team

## Ausgangslage
- Grundlage ist euer Script von der Übung "Ressource erstellen"
    - https://gitlab.com/ch-tbz-wb/Stud/azure/-/blob/main/2_Unterrichtsressourcen/Auftraege/Ressource%20erstellen.md?plain=0
- Wir gehen davon aus, dass ihr eure Ressourcen & Ressourcengruppen gelöscht habt.
- Euer Script beinhaltet aktuell Befehle für die Erstellung von Ressourcengruppen und Ressourcen.

## Anforderung
- Wir werden das Script nun schrittweise in Richtung "Automatisierung" ausbauen
- Stichworte: logging, idempotent

## Auftrag
1. Implementiert ein einfaches Logging in euer Script.
    - Mindestanforderung: Aktuelle Uhrzeit im Format "yyyyMMddThhmmss", ausgeführte Aktivität
    - Führt das Script aus. (Erwartetes Ergebnis: Script erfolgreich, Ressourcen erstellt, Logs ersichtlich)

2. Diskutiert was passiert, wenn ihr das Script nun erneut ausführt. Wird es funktionieren oder eine Fehlermeldung geben? -> entspricht eurem erwarteten Ergebnis.
    - Führt das Script erneut aus
    - Entspricht das erwartete Ergebnis dem eingetretenen Ergebnis?

3. Dokumentiere die einzelnen Schritte in deinem E-Portfolio


---
Quellen:
- [What are idempotent scripts?](https://www.stefanjudis.com/notes/what-are-idempotent-scripts/#:~:text=A%20script%20is%20idempotent%20when%20its%20repeated%20execution,seems%20logical%2C%20but%20not%20every%20script%20follows%20it.)
