# Ressource erstellen

Zeit: 90 Min.<br>
Form: 2er Team

## Anforderung

#1
- Erstelle eine neue Ressourcengruppe "myResourceGroup"
  - Region: nach eurer Wahl
- Innerhalb dieser Ressourcengruppe erstellst du eine AppService "myWebApp"
- Die Web-App wird auf einem App Service Plan mit dem Namen "myAppServicePlan" ausgeführt.
- Die Lösung soll so günstig wie möglich sein. Tipp: SKU beachten

#2
- Erstelle eine zweite Ressourcengruppe "myResourceGroupCli"
  - Region: in der entsprechenden paired Region
- Innerhalb dieser Ressourcengruppe erstellst du eine AppService "myWebAppCli"  
- Die Web-App wird auf einem App Service Plan mit dem Namen "myAppServicePlanCli" ausgeführt.
- Die Lösung soll so günstig wie möglich sein. Tipp: SKU beachten

#3
- Erstelle eine zweite Ressourcengruppe "myResourceGroupPs"
  - Region: in der entsprechenden paired Region
- Innerhalb dieser Ressourcengruppe erstellst du eine AppService "myWebAppPs"  
- Die Web-App wird auf einem App Service Plan mit dem Namen "myAppServicePlanPs" ausgeführt.
- Die Lösung soll so günstig wie möglich sein. Tipp: SKU beachten


## Auftrag

1. Erstelle die obige Anforderung #1 zuerst "manuell" im Azure-Portal. Dokumentiere die einzelnen Schritte in deinem E-Portfolio
2. Erstelle die obige Anforderung #2 per Azure CLI. Dokumentiere die einzelnen Schritte in deinem E-Portfolio
3. Erstelle die obige Anforderung #3 per Azure Powershell (Cmdlets). Dokumentiere die einzelnen Schritte in deinem E-Portfolio

---
Quellen:
- [Azure CLI installieren](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli-windows?tabs=azure-cli)
- [Azure CLI Commands](https://learn.microsoft.com/en-us/cli/azure/reference-index?view=azure-cli-latest)
- [Azure Powershell installieren](https://learn.microsoft.com/en-us/powershell/azure/install-az-ps?view=azps-9.4.0)
- [Azure Powershell anmelden](https://learn.microsoft.com/en-us/powershell/azure/authenticate-azureps?view=azps-9.4.0)
- [Azure Powershell Ressourcegroup](https://learn.microsoft.com/en-us/powershell/module/az.resources/new-azresourcegroup?view=azps-9.4.0)
- [Azure Powershell Ressource](https://learn.microsoft.com/en-us/powershell/module/az.resources/new-azresource?view=azps-9.4.0)
- [Register Ressource Provider](https://learn.microsoft.com/en-us/azure/azure-resource-manager/management/resource-providers-and-types#register-resource-provider-1)
- [Azure Ressource Providers](https://learn.microsoft.com/en-us/azure/azure-resource-manager/management/azure-services-resource-providers)
