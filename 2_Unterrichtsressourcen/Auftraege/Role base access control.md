# Role base access control

[TOC]

Da unsere Azure Student Accounts keine Zugriffsrechte auf Azure AD haben, müssen wir für dieses Thema auf die challenge labs von [learn on demand](https://msle.learnondemand.net/) ausweichen.

## Anmeldung lernondemand.net
Ruft den folgenden link auf:<br>
[https://msle.learnondemand.net/](https://msle.learnondemand.net/)

1. Meldet euch per Schlüssel an. Diesen findet ihr im MS Teams unter *files/class materials*.
   
   ![](../../x_gitressourcen/learnondemand%20training%20key.png)

2. Registriert euch. Somit könnt ihr euch beim nächsten Mal per Skillable Account anmelden.
   
   ![](../../x_gitressourcen/learnondemand%20register.png)

3. Sobald ihr eingeloggt seid, geht auf *Meine Schulung* und klickt auf die Klasse.
   
   ![](../../x_gitressourcen/learnondemand%20Klasse.png)

## challenge labs

>Wichtig: Die *challenge labs* sind zeitlich limitiert und dürfen nur 3 Mal gestartet werden.

In dieser Übung müsst ihr im Portal konfigurieren und teilweise Skripte ausführen. Haltet eure Notizen und allfällige Skripte in eurem E-Portfolio fest.

1. Geht auf *Challenge Labs supporting Module 2: Core Azure Services* und wählt den siebten challenge lab *Configure Azure Role Based Access Control [Guided]*
2. Klick auf *Starten*
3. Sobald deine Lab gestartet ist, erscheinen zwei Browser-Fenster. 
   1. Anmeldung per temporärem Account
   2. Instructions von *skillable challenges*

    ![](../../x_gitressourcen/lernondemand%20temp%20account%20skillable%20challenges.png)

4. Folgt den Anweisungen unter Instructions, *Next* klicken




