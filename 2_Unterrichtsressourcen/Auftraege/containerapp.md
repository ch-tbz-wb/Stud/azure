# Azure Container APP

Zeit: 45 Min.<br>
Form: 2er Team

## Anforderung

Ziel dieser Anforderung ist, dass du eine Container APP mit Wordpress erstellst und diese betreiben kannst.<br>
Das CMS (Content Management System) kann nur mit einer darunterliegenden Datenbank funktionieren. 

**Ziel**: Container App starten und zeigt folgendes Bild
![](../../x_gitressourcen/wordpress_successful_installed.png)

Erfülle die nachfolgenden Anforderungen zum Betrieb einer CMS!

### Erstelle eine Container APP: wordpress
- Erstelle deine App in einer bestehenden Resourcegroup, ansonsten erstelle eine Neue.
- Die Container App läuft im Region: Switzerland North
- Ersetze das bestehende hello-world Container Image durch ein neues:
  - lösche zuerst das bestehende Container Image
  - erstelle ein neues Image -> **wordpress:latest**. 
  - Image Source: **Docker Hub**
- Ingress: HTTP Zugang sollte von überall funktionieren

### Erstelle eine mySQL Datenbank
- Erstelle eine *Azure Database for MySQL* in der gleichen Resourcegroup und Region wie die Container APP
- Verwende einen *flexible server*

## Aufgaben

- Wenn du deine Container APP startest, welche Voraussetzungen für die Konfiguration des CMS Wordpress müssen erfüllt sein?
- Welche wichtigen Einstellungen beim Datenbank-Server sind für ein erfolgreiches Installieren notwendig?

## Abschluss
- Damit keine unnötigen Kosten auf deinem Students Account entstehen, entferne deine erstellten Ressourcen.

---
Quellen: <br>
https://learn.microsoft.com/de-de/azure/container-apps/overview
