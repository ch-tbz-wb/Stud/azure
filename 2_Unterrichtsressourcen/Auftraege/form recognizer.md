# Dokumentensortierer mit Form Recognizers (Azure AI Service)

[TOC]

Zeit: 4-5 Lektionen<br>
Form: Teamarbeit

## Einleitung
Im Büro sowie auch zuhause sammeln sich viele Dokumente an. Mit der Digitalisierung werden diese Dokumente nicht mehr als "Hardware" sondern elektronisch abgelegt. Damit man sie wiederfindet, legt man gut durchdachte Ordnerstrukturen in einem System an. Letztendlich werden die Dokumente in diesem Struktur kategorienweise abgelegt.

## Auftrag
Die Dokumente sollen an einem zentralen Ort gesammelt werden. Jedesmal, wenn ein Dokument hochgeladen wurde, soll das Dokument analysiert und kategorisch an einem entsprechenden Ort abgelegt werden. Dieser Prozess soll automatisiert erfolgen.

![](../../x_gitressourcen/Dokumentenverteiler%20Form%20recognizer.png)

## Lösungsfindung: Form Recognizers
Für die Umsetzung brauchst Du ein Azure AI-Service: *Form Recognizer*. 
Lese zuerst die Dokumentation und versuche zu verstehen, was eigentlich dieser Service macht.<br>
https://learn.microsoft.com/en-us/azure/applied-ai-services/form-recognizer/overview?view=form-recog-3.0.0

### Phase 1
Einfachhalber setzen wir zuerst ein prebuild-model von Microsoft ein. Hierbei kannst du die Dokumente von Microsoft verwenden.

Setzte jetzt deine Automatisierung um.

### Phase 2
Nun erstellen wir ein eigenes Model, nämlich ein *custom model*. Du erhälst einige verschiedene Dokumente, das können Rechnungen, Quittungen oder Briefe sein. 

Vorgehensweise: 
- Labeln
  - Die Dokumente haben verschieden Texte und Zeichen. Diese müssen *markiert* werden
- Mindestens 5 Dokumente sind dafür notwendig
- Anschliessend erfolgt das Training. Im Hintergrund läuft ein Modellierungsprozess ab, damit das AI-System kennenlernt, wie die Dokumente aufgebaut sind und wo sich die Labels befinden. Das kann u.U. bis zu einer Stunde dauern!

Sobald die Analyse abgeschlossen ist, kannst du danach ein Testdokument nehmen und deine erste Analyse starten. Aufgrund deines Modelles, sollten die Labels erkannt werden.

Setzte jetzt deine Automatisierung um.

## Dokumentation
Versucht eure Erkenntnisse in eurem E-Portfolio festzuhalten.

Aus Fehlern lernen: Immer wieder kommt es vor, dass wir bei der Realisierung einer Anforderung Fehler machen. Das ist ganz normal. Versucht diese Erfahrung nieder zu schreiben und somit ein *lesson learned* festzuhalten.

