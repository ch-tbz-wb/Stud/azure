# Auftrag IoT mit AZ1366

[TOC]

Zeit: 4-5 Lektionen<br>
Form: Teamarbeit

>
> Ziel dieser Übung ist, dass ihr euch mit dem Thema IoT auseinandersetzt, die dazugehörigen Ressourcetypen verwendet und die verschiedenen Azure Komponenten für eure Lösungsfindung einsetzt.
>

## Einleitung
IoT ist in aller Munde und finden in verschiedensten Bereichen ihren wertvollen Einsatz. Es gibt sogar Einsatzgebiete wo man damit nicht gerechnet hat, dass ein kleines Gerät sich aus der Ferne steuern lässt und die Daten irgendwo in die cloud hochlädt. IoT steht für "Internet of Things" (Internet der Dinge) und bezieht sich auf die Vernetzung von physischen Geräten und Gegenständen mit dem Internet. Im Wesentlichen handelt es sich um die Integration von Internetkonnektivität und Sensoren in Alltagsgegenstände, um sie intelligent, kommunikationsfähig und fernsteuerbar zu machen.

Anwendungsbereiche können sein: 
- In der Industrie bei Maschinen und Messinstrumenten
- Smart Home und Gebäudeautomatisierung
- Gesundheitswesen: Verschiedenste Medizinalgeräte
- Verkehr und Logistik
- Landwirtschaft
- Energie- und Umweltwirtschaft

Dies sind nur einige Beispiele, und die Anwendungsbereiche von IoT sind vielfältig. Im Grunde genommen kann IoT überall dort eingesetzt werden, wo die Vernetzung von Geräten und die Erfassung und Analyse von Daten zur Optimierung von Prozessen, zur Verbesserung der Effizienz oder zur Schaffung neuer Dienstleistungen und Produkte relevant sind.

## Fallbeispiel "Smart Home"
Unser Haus ist vernetzt und wir möchten gerne über verschiedensten Bewegungen informiert werden. Hierbei kann es z.B. um ein Garagentor handeln. Sobald das Tor offen ist, erhalten wir eine Nachricht.

## Inbetriebnahme von AZ3166

Hier findet ihr eine Anleitung, zur Inbetriebnahme des mxchip AZ3166:<br>
https://learn.microsoft.com/en-us/azure/iot-develop/quickstart-devkit-mxchip-az3166-iot-hub

Tip: IoT-Hub könnt ihr manuell übers Azure Portal erfassen, oder gem. Anleitung in obigem Link.

Den source code für die Firmware findet ihr hier:<br>
https://github.com/azure-rtos/getting-started/tree/master/MXChip/AZ3166/app

Wissenswertes über IoT-Hub findet ihr hier:<br>
https://learn.microsoft.com/en-us/azure/iot-hub/iot-concepts-and-iot-hub


## Einsatz AZ3166 mit Magnetsensor: Szenario "Bewegungsmelder für Smart Home"
Ihr habt jetzt das Gerät bereit gestellt und die Daten werden über das [MQTT Protokoll](https://mqtt.org/) an IoT-Hub gesendet.
Diese Messages könnt ihr grundsätzlich weiterverarbeiten, was eigentlich auch Ziel dieser Übung ist.

Das Termite-Tool zeigt die gesendeten Messages an. Unter anderem sind die unterschiedlichen Messwerte erkennbar.

![](../../x_gitressourcen/Termite%20tool.png)

Für unseren zweck interessieren uns eigentlich nur die magnetischen Werte.

```json
{"gyroscopeX":-70,"gyroscopeY":0,"gyroscopeZ":0}.
```

### Lösungsansatz
Wir möchten unsere Lösung Event-basierend umsetzen. Hierzu ist ein Event-Grid notwendig, damit wir die gesendeten Messages weiterleiten können.
Event-Grid arbeiten nach dem Prinzip "Publisher-Subscriber". Wissenswertes könnt ihr hier lesen:
https://learn.microsoft.com/en-us/azure/event-grid/overview

![](../../x_gitressourcen/4-event-grid.png)

Publisher ist in unserem Fall unser IoT-Hub und als Subscriber legen wir eine Storage Queue fest. 
Dazu müsst ihr zuerst einen Storage Account und eine dazugehörige Queue anlegen.

Danach müsst ihr einen "Event-Grid-System-Topic" erstellen. Findet selbst heraus, was der Unterschied zwischen "Event-Grid-System-Topic" und "Event-Grid-Topic" ist.

Anschliessend kann im IoT-Hub unter *Events* eine Subscription erfasst werden. Hierbei müsst ihr nur noch den Endpoint für den Subscriber festlegen, Storage Account Queue. Jetzt sollten die Messages in der Queue ankommen.

Damit wir nun eine Nachricht, z.B. eine E-Mail, erhalten, erstellen wir nun eine Logic App. Hier wird nach dem Polling-Prinzip die Queue überprüft und bei neuen Messages wird die Logic APP getriggert. Mit diesem Trigger werden Messages aus der Queue gelesen und die Magnetwerte geprüft. Sobald Werte über- oder unterschritten werden, wird eine E-Mail gesendet.

![](../../x_gitressourcen/Loesungsfindung%20Smart%20Home.png)

## Monitoring der gemessenen Werte
Microsoft stellt eine zentrale Applikationsplattform zur Verfügung. Hier kann man seine persönliche Applikation generieren und u.a. sein Dashboard konfigurieren.

Erstelle wie auf diesem Bild ein Dashboard, das deine gemessene Werte des mxchip's anzeigt.

![](../../x_gitressourcen/iot%20central.png)

Folge dieser Anleitung wie du eine Applikation im IoT-Central erstellst.
https://learn.microsoft.com/en-us/azure/iot-develop/quickstart-devkit-mxchip-az3166

## Dokumentation
Versucht eure Erkenntnisse in eurem E-Portfolio festzuhalten.

Aus Fehlern lernen: Immer wieder kommt es vor, dass wir bei der Realisierung einer Anforderung Fehler machen. Das ist ganz normal. Versucht diese Erfahrung nieder zu schreiben und somit ein *lesson learned* festzuhalten.


