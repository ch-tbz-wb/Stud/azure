# Azure storage account

Zeit: 45 Min.<br>
Form: 2er Team

## Anforderung

### Erstelle ein storage account in einer eigenen resourcegroup.
- **least privilege** Ansatz

- Folgende settings muss dein storage account mind. enthalten:<br>

|Region|Performance|Redundancy|
|------|-----------|----------|
|Switzerland North|Standard|LRS|

- Erstelle zwei blob-container
   - container1: Enthält nur Bilder
   - container2: Enthält nur Textfiles
- Damit der Zugriff auf dein Storage Account geschützt ist, erstelle zuerst ein [Account SAS (Shared Access Signature)](https://learn.microsoft.com/en-us/azure/storage/common/storage-sas-overview?toc=%2Fazure%2Fstorage%2Fblobs%2Ftoc.json&bc=%2Fazure%2Fstorage%2Fblobs%2Fbreadcrumb%2Ftoc.json)

   - Services: Blob
   - Zugriff ist nur für einen Tag gültig
   - Signing key: Verwende für die Generierung key2
   - Konfiguriere die richtigen resource types!

## Aufgaben

### Azure storage explorer
1. Installiere das Tool [Azure Storage Explorer](https://azure.microsoft.com/en-us/products/storage/storage-explorer/).
2. Erstelle eine Connection zu deinem storage account mit deinem *generierten SAS Token URL*
3. Lade ein paar files in deine containers hoch
4. Dokumentiere dein Vorgehen in deinem E-Portfolio


### Azure CLI oder Powershell
- Generiere einen SAS Token mit CLI/ PS
- Erstelle eine Verbindung wiederum mit deinem *generierten SAS Token URL*
- Lade weitere files hoch mit dem azcopy Befehl/ azure powershell
- Dokumentiere dein Skript in deinem E-Portfolio


### Fragen
- In welchen Fällen sollte man *Account SAS* verwenden?
- Welche andere Connection-Möglichkeiten bietet das Tool *Azure Storage Explorer* an?
- Weshalb gibt es bei storage account zwei signing keys?
- Gibt es Empfehlungen/ best practices für die Verwendung von SAS/ Storage keys?
- Beischreibe, wie du die Anforderung *least privilege* umgesetzt hast.

## Abschluss
- Damit keine unnötigen Kosten auf deinem Students Account entstehen, entferne deine erstellten Ressourcen.
