# Unterrichtsressourcen 

[Modulübersicht Azure](Moduluebersicht%20Azure.pdf)

* [Modul01](Modul01/)
* [Modul02](Modul02/)
* [Modul03](Modul03/)



# Linksammlung
* [Azure Subscription Service Limits](https://learn.microsoft.com/en-us/azure/azure-resource-manager/management/azure-subscription-service-limits)
* [Comparing Container Apps with other Azure container options](https://learn.microsoft.com/en-us/azure/container-apps/compare-options)
* [Save Compute Costs Reservations](https://learn.microsoft.com/en-us/azure/cost-management-billing/reservations/save-compute-costs-reservations)
* [Microsoft Cloud Adoption Framework](https://learn.microsoft.com/en-us/azure/cloud-adoption-framework)
* [Accelerate migration](https://learn.microsoft.com/en-us/azure/cloud-adoption-framework/get-started/migrate)
* [Azure Architectures](https://learn.microsoft.com/en-us/azure/architecture/browse/)
* [Azure Well-Architected Framework](https://learn.microsoft.com/en-us/azure/architecture/framework/)
* [Microsoft Zero Trust Guidance Center](https://learn.microsoft.com/en-us/security/zero-trust/)
