# kubernetes

>TODO: Dokumentation noch in Bearbeitung

Kubernetes ist eine Open-Source-Container-Orchestrierungsplattform, die für die Automatisierung der Bereitstellung, Skalierung und Verwaltung von Anwendungen in Containern entwickelt wurde. Kubernetes wurde ursprünglich von Google entwickelt und später der Cloud Native Computing Foundation (CNCF) übergeben.

Kubernetes ermöglicht es Entwicklern und IT-Teams, Container-Anwendungen unabhängig von der zugrunde liegenden Infrastruktur bereitzustellen und zu betreiben. Es bietet eine Vielzahl von Funktionen, die die Verwaltung von Containern erleichtern, darunter automatische Skalierung, Lastenausgleich, automatisierte Rollouts, Rollbacks und Self-Healing.

Kubernetes basiert auf einem Master-Slave-Modell, bei dem ein Master-Knoten für die Verwaltung und Überwachung der Slave-Knoten (auch als Worker-Knoten bezeichnet) zuständig ist. Der Master-Knoten überwacht die Gesundheit der Anwendungen und der Worker-Knoten, weist Aufgaben an die Worker-Knoten zu und sorgt dafür, dass die Anwendungen im gewünschten Zustand ausgeführt werden.

Kubernetes unterstützt eine Vielzahl von Containertechnologien, einschließlich Docker, und kann auch mit anderen Cloud-Technologien wie OpenStack und AWS integriert werden. Kubernetes hat sich als eine der führenden Plattformen für die Bereitstellung und Verwaltung von Containern etabliert und wird von vielen Unternehmen und Organisationen in der Produktion eingesetzt.

# Ingress (u.a. Reverse-Proxy)
Ein Reverse-Proxy ist ein Server, der als Vermittler zwischen einem Client und einem oder mehreren Servern fungiert. Im Gegensatz zu einem herkömmlichen Proxy, der als Vermittler zwischen einem Client und einem Server fungiert, kann ein Reverse-Proxy für mehrere Server in verschiedenen Netzwerken gleichzeitig arbeiten. Der Reverse-Proxy empfängt die Anfragen von den Clients und leitet sie an den entsprechenden Server weiter, um die Antworten zurück an den Client zu senden.

Ein Reverse-Proxy kann für verschiedene Zwecke eingesetzt werden, wie zum Beispiel für Lastenausgleich, Schutz von Servern vor direktem Zugriff aus dem Internet, Verbesserung der Geschwindigkeit von Webanwendungen durch Caching und Verringerung von Latenzen durch den Einsatz von geografisch verteilten Servern.

Ein Beispiel für die Verwendung eines Reverse-Proxys ist die Verwendung von NGINX als Reverse-Proxy für eine Node.js-Anwendung. Der Reverse-Proxy empfängt die HTTP-Anfragen von den Clients und leitet sie an den Node.js-Server weiter, der die Anfrage verarbeitet und die Antwort an den Reverse-Proxy zurücksendet. Der Reverse-Proxy kann dann die Antwort zwischenspeichern, um zukünftige Anfragen schneller zu beantworten.

![](../../x_gitressourcen/Ingress.webp)


---
Quellen: <br>
- https://kubernetes.io/de/
- https://www.solo.io/topics/kubernetes-api-gateway/kubernetes-ingress/
