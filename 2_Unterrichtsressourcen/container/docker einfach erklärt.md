
# Docker einfach erklärt

## Was sind docker container?
Docker Container sind eine Art light-Version von Virtuellen Maschinen. 

Während Virtuelle Maschinen die Hardware eines Rechners, also den Prozessor, die Festplatte, usw. virtualisieren (Hypervisor), virtualisieren Docker Container das Betriebssystem. Dies wird auch Containervirtualisierung genannt. Aber was bedeutet das?

Containervirtualisierung bedeutet, dass basierend auf einem auf der Maschine laufenden Host-Betriebssystems (z.B. Ubuntu) weitere Linux Distributionen wie CentOS, Debian oder auch Ubuntu, parallel betrieben werden können. Der Trick: es wird der Kern des Host-Betriebssystems (der Kernel) mit den Containern geteilt.

![docker_vs_vm](../../x_gitressourcen/docker_vs_vm.webp)

Dabei kann das Host-Betriebssystem den Zugriff auf Dateien, Hardware, Speicher und Geräte beliebig einschränken. Womit die einzelnen Container voneinander isoliert laufen können.

[Video Erklärung](https://www.youtube.com/watch?v=5GanJdbHlAA&list=PLy7NrYWoggjwPggqtFsI_zMAwvG0SqYCb&index=6)

## Vorteile eines Docker Containers?
Der wesentliche Vorteil von Containern ist es, dass diese sowohl deutlich weniger Speicher benötigen, als auch deutlich schneller laufen. Man muss für das Hochfahren eines neuen Containers nicht ein komplettes VM-Image mit mehreren GB laden, sondern kann mit abgespeckten Linux Distributionen wie Alpine Linux bereits mit wenigen MB starten.

Container können sowohl lokal auf deinem Laptop mit der Anwendung Docker gemanaged werden, als auch in der Cloud betrieben werden.

Doch du musst nicht von 0 weg starten. Es gibt ein globales Verzeichnis namens Docker Hub (Repository) wo bereits fertige Container-Images mit den unterschiedlichsten Applikationen wie Webservern, Datenbanken, usw. bereits verfügbar sind und mit einem Klick bei dir gestartet werden können.

Docker Container stellen somit eine schnelle und kostengünstige Variante dar, die unterschiedlichsten Web- und Offline-Anwendungen in Bereichen wie der Logistik zu betreiben. Diese können innerhalb von Sekunden gestartet, gestoppt und auch wieder gelöscht werden.


## Was sind Images?
Der Begriff „Image“ ist Ihnen im Zusammenhang mit Virtualisierung möglicherweise von virtuellen Maschinen (VMs) bekannt. Für gewöhnlich handelt es sich bei einem VM-Image um eine Kopie eines Betriebssystems. Ggf. enthält ein VM-Image weitere installierten Komponenten wie Datenbank und Webserver. Der Begriff entstammt einer Zeit, in der Software auf optischen Datenträgern wie CD-ROMs und DVDs verteilt wurde. Wollte man eine lokale Kopie des Datenträgers anlegen, erstellte man mit einer speziellen Software ein Abbild, auf Englisch „Image“.

Bei der Container-Virtualisierung handelt es sich um die konsequente Weiterentwicklung der VM-Virtualisierung. Anstatt einen virtuellen Computer (Maschine) mit eigenem Betriebssystem zu virtualisieren, umfasst ein Docker-Image in der Regel lediglich eine Anwendung. Dabei kann es sich um eine einzelne Binärdatei handeln oder um einen Verbund mehrerer Software-Komponenten.

Ein Container-Image ist ein Binärdatei-Format. Es ist eine Sammlung von Dateisystemen und Konfigurationsinformationen, die in einem einzigen Paket gebündelt sind und in einem Container ausgeführt werden können. Container-Images können in der Regel nicht direkt als Textdatei gelesen werden, da sie in einem bestimmten Format kodiert sind.

Das Container-Image-Format kann unterschiedlich sein, je nachdem welches Tool zur Erstellung des Images verwendet wird. Ein gängiges Format für Container-Images ist das Docker-Image-Format, das normalerweise eine Binärdatei im tar-Format ist, die mehrere Schichten von Dateisystemen und Konfigurationsinformationen enthält. Andere Container-Image-Formate, wie das OCI-Image-Format, können ebenfalls binäre Formate sein.

Obwohl Container-Images nicht direkt als Textdatei gelesen werden können, können bestimmte Informationen aus dem Container-Image extrahiert werden. Zum Beispiel können Sie mit dem Befehl *docker inspect* Informationen wie Metadaten, Umgebungsvariablen und die Liste der in einem Image enthaltenen Dateien abrufen.

Um die Anwendung auszuführen, wird aus dem Image zunächst ein Container erzeugt. Alle auf einem Docker-Host laufenden Container greifen auf denselben Betriebssystem-Kernel zurück. Dadurch sind Docker-Container und Docker-Images in der Regel deutlich leichtgewichtiger als vergleichbare virtuelle Maschinen und deren Images.

Die Konzepte Docker-Container und Docker-Image sind eng miteinander verknüpft. So kann nicht nur ein Docker-Container aus einem Docker-Image erzeugt werden, sondern auch aus einem laufenden Container ein neues Image.

## Weitere Begriffe aus der container Technologie

### docker deamon
Der Docker Deamon ist ein dauerhafter Hintergrundprozess, der Docker-Images, Container, Netzwerke und Speichervolumen verwaltet. Er „schaut“ ständig auf Docker-API-Anforderungen und verarbeitet sie. Der Docker-Daemon ist der Kern von Docker und bildet das Herzstück der Container-Engine.

### docker client
Der Docker-Client ermöglicht den Benutzern die Interaktion mit Docker. Er kann sich auf demselben Host wie der Daemon befinden oder eine Verbindung zu einem Daemon auf einem entfernten Host herstellen und mit mehr als einem Daemon kommunizieren. Der Docker-Client bietet eine Befehlszeilenschnittstelle (CLI = Command Line Interpreter), über die Sie einem Docker-Daemon Befehle zum Erstellen, Ausführen und Anhalten von Anwendungen erteilen können.

Der Hauptzweck des Docker-Clients besteht darin, ein Mittel zur Verfügung zu stellen, mit dem Images aus einer Registry gezogen und auf einem Docker-Host ausgeführt werden können. Befehle werden nachfolgend unter **commands/ Referenzen** erklärt.

### Docker Registry

In Docker Registries werden Images versioniert abgelegt und verteilt.

Die Standard-Registry ist der **Docker Hub**, auf dem tausende öffentlich verfügbarer Images zur Verfügung stehen, aber auch "offizielle" Images.

Viele Organisationen und Firmen nutzen eigene Registries, um kommerzielle oder "private" Images zu hosten, aber auch um den Overhead zu vermeiden, der mit dem Herunterladen von Images über das Internet einhergeht.

![docker archtecture](../../x_gitressourcen/docker%20architecture.svg)

## Commands/Referenzen

Der Docker Client bietet eine Vielzahl von Befehlen, die für die Bedienung der Anwendung genutzt werden können. In diesem Abschnitt werden daher jene Befehle etwas näher beleuchtet.

**docker run**

- Ist der Befehl zum Starten neuer Container.
- Der bei weitem komplexesten Befehl, er unterstützt eine lange Liste möglicher Argumente.
- Ermöglicht es dem Anwender, zu konfigurieren, wie das Image laufen soll, Dockerfile-Einstellungen zu überschreiben, Verbindungen zu konfigurieren und Berechtigungen und Ressourcen für den Container zu setzen.


Standard-Test:

    $ docker run hello-world
Startet einen Container mit einer interaktiven Shell (interactive, tty):

    $ docker run -it ubuntu /bin/bash
Startet einen Container, der im Hintergrund (detach) läuft:

    $ docker run -d ubuntu sleep 20
Startet einen Container im Hintergrund und löscht (remove) diesen nach Beendigung des Jobs:

    $ docker run -d --rm ubuntu sleep 20
Startet einen Container im Hintergrund und legt eine Datei an:

    $ docker run -d ubuntu touch /tmp/lock
Startet einen Container im Hintergrund und gibt das ROOT-Verzeichnis (/) nach STDOUT aus:

    $ docker run -d ubuntu ls -l
docker ps

Gibt einen Überblick über die aktuellen Container, wie z.B. Namen, IDs und Status.
Aktive Container anzeigen:

    $ docker ps
Aktive und beendete Container anzeigen (all):

    $ docker ps -a
Nur IDs ausgeben (all, quit):

    $ docker ps -a -q
docker images

- Gibt eine Liste lokaler Images aus, wobei Informationen zu Repository-Namen, Tag-Namen und Grösse enthalten sind.


Lokale Images ausgeben:

    $ docker images
Alternativ auch mit ... image ls:

    $ docker image ls
docker rm und docker rmi

docker rm
- Entfernt einen oder mehrere Container. Gibt die Namen oder IDs erfolgreich gelöschter Container zurück.
docker rmi
- Löscht das oder die angegebenen Images. Diese werden durch ihre ID oder Repository- und Tag-Namen spezifiziert.


Docker Container löschen:

    $ docker rm [name]
Alle beendeten Container löschen:

    $ docker rm `docker ps -a -q`
Alle Container, auch aktive, löschen:

    $ docker rm -f `docker ps -a -q`
Docker Image löschen:

    $ docker rmi ubuntu
Zwischenimages löschen (haben keinen Namen):

    $ docker rmi `docker images -q -f dangling=true`

docker start

- Startet einen (oder mehrere) gestoppte Container.
    - Kann genutzt werden, um einen Container neu zu starten, der beendet wurde, oder um einen Container zu starten, der mit docker create erzeugt, aber nie gestartet wurde.


Docker Container neu starten, die Daten bleiben erhalten:

    $ docker start [id]

**Container stoppen, killen**

docker stop
- Stoppt einen oder mehrere Container (ohne sie zu entfernen). Nach dem Aufruf von docker stop für einen Container wird er in den Status »exited« überführt.

docker kill
- Schickt ein Signal an den Hauptprozess (PID 1) in einem Container. Standardmässig wird SIGKILL gesendet, womit der Container sofort stoppt.

**Informationen zu Containern**

docker logs
- Gibt die "Logs" für einen Container aus. Dabei handelt es sich einfach um alles, was innerhalb des Containers nach STDERR oder STDOUT geschrieben wurde.

docker inspect
- Gibt umfangreiche Informationen zu Containern oder Images aus. Dazu gehören die meisten Konfigurationsoptionen und Netzwerkeinstellungen sowie Volumes-Mappings.

- docker diff
Gibt die Änderungen am Dateisystem des Containers verglichen mit dem Image aus, aus dem er gestartet wurde.

docker top
- Gibt Informationen zu den laufenden Prozessen in einem angegebenen Container aus.

---
**Quellen**

- https://docs.docker.com/get-started/overview/
- https://docs.docker.com/engine/reference/commandline/docker/





