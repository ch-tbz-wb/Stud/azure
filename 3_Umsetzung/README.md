# Umsetzung
- Bereich: Automatisierung, Bereitstellung von Azure Ressourcen
- Semester: 1

## Lektionen
- Präsenzunterricht: 20
- Fernunterricht: 10
- Selbststudium: 20

## Lernziele
- Modul 1 – Cloudkonzepte
  - Cloud Computing
    - Was ist Cloud Computing?
    - Gemeinsame Verantwortung
    - Cloudmodelle
    - Kapital im Vergleich zu Betriebskosten
  - Vorteile der Cloud
  - Clouddiensttypen
    - IaaS, PaaS und SaaS
- Modul 2: Azure-Architektur und -Dienste
  - Azure-Architekturkomponenten
    - Regionen und Verfügbarkeitszonen
    - Abonnements und Ressourcengruppen
  - Compute und Netzwerk
    - Computetypen
    - Anwendungshosting
    - Virtuelle Netzwerke
  - Storage
    - Speicherdienste
    - Redundanzoptionen
    - Dateiverwaltung und -migration
  - Identität, Zugriff und Sicherheit
    - Verzeichnisdienste
    - Authentifizierungsmethoden
    - Sicherheitsmodelle
- Modul 3: Azure-Verwaltung und -Governance
  - Kostenverwaltung
    - Kosten- und Preisrechner
    - Kostenverwaltung und Tags
  - Governance und Einhaltung
    - Blaupausen, Richtlinien und Ressourcensperren
    - Service Trust Portal
  - Ressourcenbereitstellungstools
    - Portal, PowerShell, CLI und andere
    - Azure Arc und der Azure Resource Manager
  - Überwachungstools
    - Der Azure Advisor, Azure Service Health und Azure Monitor

## Voraussetzungen

Modul [122 Abläufe mit einer Scriptsprache automatisieren](https://www.modulbaukasten.ch/module/122) aus der Grundbildung

## Dispensation

Zertifizierung [Microsoft Certified: Azure Fundamentals](https://learn.microsoft.com/en-us/credentials/certifications/azure-fundamentals/) oder höher

## Technologien

Powershell, Azure CLI, ARM-Templates, Biceps, Visual Studio Code

## Methoden

Self-Learning Plattformen und praktische Laborübungen, Coaching durch Lehrperson

## Schlüsselbegriffe

Skriptsprachen, Automatisierung, Cloud Monitoring Konzepte, Blueprints, Compute Services, IAM, RBAC, Regions, Availability Zones, Datacenter

## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Workshop, Gastreferenten, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen

## Lehrmittel

Alle Lehrmittel sind in diesem Repository verlinkt.

## Hilfsmittel




