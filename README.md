![TBZ Logo](./x_gitressourcen/tbz_logo.png) 

# AZURE - Cloud Computing Services with Microsoft Azure

## Kurzbeschreibung des Moduls 

In diesem Kurs werden Azure und Microsoft-Dienste vorgestellt, mit denen Cloud Computing-Lösungen erstellbar sind. Dieser Kurs vermittelt grundlegende Kenntnisse über Azure-Konzepte, wichtige Azure-Dienste, grundlegende Lösungen und Verwaltungstools, allgemeine Sicherheit und Netzwerksicherheit, Features für Governance, Datenschutz und Compliance, Azure-Kostenverwaltung und Vereinbarungen zum Servicelevel.

Als zertifizierte Azure Spezialisten mit viel praktischer Erfahrung, begleiten wir dich in diesem Kurs als Coaches, geben dir Tipps & Tricks, Ratschläge sowie Story-Telling aus der Praxis.

Aus diesem Grund gestalten wir den Unterricht mit viel „Hands-on“ und ergänzt mit selbstgesteuertem Lernen. Finde dein eigenes Lerntempo: Als Azure-Coaches stehen wir dir beratend zur Seite und bieten individuelle Unterstützung.

## Angaben zum Transfer der erworbenen Kompetenzen 

- Self-Study Azure Fundamentals Kurs mit Coaching durch Lehrpersonen
- Praktische Übungen und Challenge Aufgaben mit Azure Services inkl. Einführung in Azure-AI-Services und IOT-Devices

## Abhängigkeiten und Abgrenzungen

### Vorangehende Module

- Scriptingkenntnisse von Vorteil
- Empfehlung: Vorkurs Cloud-native, DevOps und Container – Dipl. Informatiker/in HF

### Nachfolgende Module

## Dispensation

Gemäss Reglement HF Lehrgang

## Unterlagen zum Modul

### Organisatorisches

[Organisatorisches ](0_Organisatorisches)zur Autorenschaft dieser Dokumente zum Modul

## Handlungskompetenzen

[Handlungskompetenzen](1_Handlungskompetenzen) zu den Handlungszielen 

### Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen

### Umsetzung

[Umsetzung](3_Umsetzung) 

### Fragekatalog

[Fragekatalog ](4_Fragekatalog) Allgemein

### Handlungssituationen

[Handlungssituationen ](5_Handlungssituationen)mit möglichen Praxissituationen zu den einzelnen Handlungszielen

### Zertifizierungen 

Mögliche [Zertifizierungen](8_Zertifizierungen) für dieses Modul 

- - - 

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>.

- - - 
